// include required header file
#include "Item.h"

// constructor definition 
Item::Item(const std::string& name, const std::string& description, ItemType type, int price) : name(name), description(description), type(type), price(price) {}

std::string Item::getName()
{
    // return name of an item
	return name;
}
std::string Item::getDescription() 
{
    // return description of an item
	return description;
}
int Item::getPrice() 
{
    // return price of an item
	return price;
}
Item::ItemType Item::getItemTypeFromString(const std::string& typeStr)
{
    // check if item type string matches the known item types
    if (typeStr == "Weapon") 
    {
        // return enum value for Weapon type
        return ItemType::Weapon;
    }
    else if (typeStr == "Armour") 
    {
        // return enum value for Armour type
        return ItemType::Armour;
    }
    else if (typeStr == "Potion") 
    {
        // return enum value for Potion type
        return ItemType::Potion;
    }
    else 
    {
        // return enum value for Unknown type (if doesnt match any known types)
        return ItemType::Unknown;
    }
}
Item::ItemType Item::getItemType() const
{
    // return type of an item
    return type;
}
