#pragma once
// include required library
#include <string>

class Item
{
    public:
        // enumeration for item types
        enum class ItemType { Weapon, Armour, Potion, Unknown };

    protected:
        std::string name; // string for name of item
        std::string description; // string for description of item
        ItemType type; // type of item
        int price; // int for price of item

    public:
        Item(const std::string& name, const std::string& description, ItemType type, int price); // constructor to initalise the item
        std::string getName(); // function to get the name of an item
        std::string getDescription(); // function to get the description of an item
        int getPrice(); // function to get the price of an item
        ItemType getItemType() const; // function to get the type of an item
        static ItemType getItemTypeFromString(const std::string& typeStr); // function to convert string representation of item type to enum ItemType
        virtual int getStatChange() const { return 0; } // function to get stat change of item (is gonna be overridden)
};
