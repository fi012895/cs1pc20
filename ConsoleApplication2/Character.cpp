#include "Character.h"

#include <string>
#include <iostream>
#include <map>
#include <vector>

// constructor definition
Character::Character(const std::string& name, int health, int defense, int damage, int coins, std::string description, Room* location)
		  : name(name), health(health), defense(defense), damage(damage), coins(coins), description(description), location(location) {}

Room* Character::getLocation()
{
	// return characters location
	return location;
}
void Character::setLocation(Room* loc)
{
	// set characters location to input loc
	location = loc;
}
std::string Character::getName()
{
	//return characters name
	return name;
}
std::string Character::getDescription()
{
	//return characters description
	return description;
}
int Character::getHealth()
{
	//return characters health
	return health;
}
int Character::getDefense()
{
	//return characters defense
	return defense;
}
int Character::getDamage()
{
	// return characters damage
	return damage;
}
int Character::getCoins()
{
	// return characters coins
	return coins;
}
void Character::setHealth(int Health)
{
	// set health to input Health
	health = Health;
}
void Character::takeDamage(int damage)
{
	// attack dealing damage to health
	health = health - damage;
}
bool Character::isAlive()
{
	// return boolean (true (health > 0) or false (health <= 0) )
	return health > 0;
}
void Character::setCoins(int money, int PosOrNeg)
{
	// set coins using money * (1 (increment) or -1 (decrement) )
	coins = coins + (PosOrNeg * money);
}

void Character::displayStats()
{
	// output stats health, attack, defense
	std::cout << getName() << " Stats - Health: " << getHealth() << ", Attack: " << getDamage() << ", Defense: " << getDefense() << std::endl;
}
