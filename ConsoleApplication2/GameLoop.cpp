// include required header files
#include "Area.h"
#include "Player.h"
#include "CommandInterpreter.h"
// include required libraries
#include <iostream>
#include <string>
#include <map>
#include <vector>
#include <cstdlib>

// add all comments
// write report
// finished, due: 19th March 2024

// function to clear the console screen
void clearScreen()
{
    std::system("cls");
}
int main()
{
    // load the game contents from files:
    Area area; // create Area
    area.LoadMapFromFile("game_map.txt"); // load area map from file
    std::vector<Item*> availableItems = area.readItemsFromFile("game_map_items.txt"); // load items from file and store in vector
    area.randomlyAssignItemsToRooms(availableItems); // randomly assign items to rooms from vector
    std::vector<Enemy*> availableEnemies = area.readEnemiesFromFile("game_map_enemies.txt"); // load enemies from file and store in vector
    area.randomlyAssignEnemiesToRooms(availableEnemies); // randomly assign enemies to rooms from vector
    NpcMerchant merchant("Merchant", "game_npc_merchant_dialogue.txt", 500); // create instance of NpcMerchant class and give values
    area.randomlyAssignNPCsToRooms(merchant); // randomly assign merchant created to random room
    
    // create Player
    Player player("Hero", "A noble warrior", area.getRoom("startRoom"), 10, 5, 150);
    // create CommandInterpreter
    CommandInterpreter commandInterpreter(&player, area.getRooms(), &merchant);
    // call function to clear screen from debugging messages
    clearScreen();
    // game loop
    while (true)
    {
        // prompt the player for a command
        std::cout << "What would you like to do? ('help' for command list) ";
        std::string command;
        std::cin >> command;
        // interpret the players command
        commandInterpreter.interpretPlayerCommand(command);
        // check if the player wants to exit the game
        if (command == "exit")
        {
            std::cout << "Exiting the game. Goodbye!\n";
            break;
        }
        // check if player is dead
        if (player.getHealth() <= 0) 
        {
            std::cout << "You have lost...\n";
            std::cout << "Exiting the game. Goodbye!\n";
            break;
        }
    }
    return 0;
}
