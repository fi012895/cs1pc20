#pragma once
// include required header files
#include "Room.h"
// include required libraries
#include <string>

// class character, responsible for handling the different character types
class Character
{
    protected:
        std::string name; // string for name of character 
        int health; // int for health of character
        int defense; // int for defense of character
        int damage; // int for damage of character
        int coins; // int for coins of character
        std::string description; // string for description of character 
        Room* location; // Room* for location of character 

    public:
        Character(const std::string& name, int health, int defense, int damage, int coins, std::string description, Room* location); // constructor to initialise character
        std::string getName(); // function to get characters name
        Room* getLocation(); // function to get characters location
        void setLocation(Room* Location); // function to set characters location
        std::string getDescription(); // function to get characters description
        int getHealth(); // function to get characters health
        virtual int getDefense(); // function to get characters defense
        virtual int getDamage(); // function to get characters damage
        int getCoins(); // function to get characters coins
        void setHealth(int Health); // function to set characters health
        void takeDamage(int damage); // function to do damage to characters health
        bool isAlive(); // function to check if character is alive, health > 0
        void setCoins(int money, int PosOrNeg); // function to set characters coins
        virtual void displayStats(); // function to display characters stats

};