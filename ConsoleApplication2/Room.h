#pragma once
// include required header file
#include "Item.h"
// include required libaries
#include <string>
#include <iostream>
#include <map>
#include <vector>
// forward declaration of enemy class and npc class
class Enemy;
class Npc;

// class room, responsible for handling the different rooms and their content
class Room
{
    private:
        std::string description; // string for description of room
        std::map<std::string, Room*> exits; // map for exits of room
        std::vector<Item*> items; // vector of pointers to items in room
        std::vector<Enemy*> enemies; // vector of pointers to enemies in room
        std::vector<Npc*> npcs; // vector of pointers to npcs in room

    public:
        Room(const std::string& description); // constructor to initialise room
        std::string getDescription(); // function to get description of room
        void AddItem(Item* item); // function to add items to room
        void RemoveItem(Item* item); // function to remove items from room
        void AddEnemy(Enemy* enemy); // function to add enemies to room
        void RemoveEnemy(Enemy* enemy); // function to remove enemies from room
        void AddNpc(Npc* npc); // function to add npcs to room
        std::vector<Npc*> getNPCs(); // function to get npcs from room
        std::vector<Item*> getItems(); // function to get items from room
        std::vector<Enemy*> getEnemies(); // function to get enemies from room
        std::map<std::string, Room*>& getExits(); // function to get exits from room
        void addExit(const std::string& direction, Room* room); // function to add an exit to another room
        Room* getExit(const std::string& direction); // function to get exit from room
        void printExits(); // function to print exits of room
        
};
