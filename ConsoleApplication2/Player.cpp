// include required header files
#include "Player.h"
#include "Room.h"
#include "Potion.h"
#include "Weapon.h"
#include "Armour.h"

// constructor definition
Player::Player(const std::string& name, std::string description, Room* location, int damage, int defense, int health)
	: Character(name, health, defense, damage, coins, description, location) {}

std::vector<Item*>& Player::getInventory() 
{
    // return players inventory
	return inventory;
}
void Player::addItemToInventory(Item& item) 
{
    // add item to players inventory using item
    inventory.push_back(&item);
    // debug message, for successfully adding item to inventory
    std::cout << "Added " << item.getName() << " to inventory" << std::endl;
}
void Player::removeItemFromInventory(const std::string& itemName) 
{
    // iterate through players inventory
    for (auto it = inventory.begin(); it != inventory.end(); ++it) 
    {
        // check if items name matches specified item name
        if ((*it)->getName() == itemName)
        {
            // remove item from inventory
            inventory.erase(it);
            // debug message, for successfully remove item from inventory
            std::cout << "Removed " << itemName << " from inventory" << std::endl;
            return;
        }
    }
    // debug message, for item not found in inventory from itemname
    std::cout << itemName << " not found in inventory" << std::endl;
}
void Player::displayInventory()
{
    // output heading for inventory
    std::cout << "Inventory:" << std::endl;
    int index = 1;
    // iterate through each item in inventory
    for (auto& item : inventory)
    {
        // output details of each item
        std::cout << index << ": " << item->getName() << " - " << item->getDescription() << 
            " - Stat Change: " << item->getStatChange() << " - Price: " 
           << item->getPrice() << std::endl;
        index++;
    }
}
void Player::displayStats()
{
    // output stats of player
    std::cout << getName() << " Stats - Health: " << getHealth() << ", Attack: " << getDamage() << ", Defense: " << getDefense() << ", Coins: " << getCoins() << std::endl;
}
void Player::updateStats()
{
    // reset stats to base values
    currentDamage = damage;
    currentDefense = defense;

    // update stats based on equipped items
    for (const auto& pair : equipment) 
    {
        const std::string& slotName = pair.first;
        Item* item = pair.second;

        if (item != nullptr) 
        {
            if (slotName == "weapon" && item->getItemType() == Item::ItemType::Weapon) 
            {
                // update damage if item is a weapon
                currentDamage += static_cast<Weapon*>(item)->getStatChange();
            }
            else if (slotName == "armour" && item->getItemType() == Item::ItemType::Armour) 
            {
                // update defense if item is armour
                currentDefense += static_cast<Armour*>(item)->getStatChange();
            }
        }
    }
}
int Player::getDefense()
{
    // return players current defense
    return currentDefense;
}
int Player::getDamage()
{
    // return players current damage
    return currentDamage;
}
bool Player::getPlayerDefending()
{
    // return if player is defending or not
    return playerDefending;
}
void Player::setPlayerDefending(bool defending)
{
    // set if player is defending or not using defending
    playerDefending = defending;
}
void Player::investigate()
{
    // output current room description
    std::cout << "You are in: " << location->getDescription() << std::endl;
    // output available room exits
    location->printExits();
    // get items in the room
    std::vector<Item*> items;
    for (Item* item : location->getItems())
    {
        if (item != nullptr)
        {
            items.push_back(item);
        }
    }
    // check for items in the room
    if (!items.empty())
    {
        std::cout << "Items in the room:" << std::endl;
        for (Item* item : items)
        {
            if (item != nullptr)
            {
                std::cout << "- " << item->getName() << ", " << item->getDescription() << std::endl;
            }
        }
    }
    else 
    {
        std::cout << "There are no items in the room." << std::endl;
    }
    // get enemies in the room
    std::vector<Enemy*> enemies;
    for (Enemy* enemy : location->getEnemies())
    {
        if (enemy != nullptr)
        {
            enemies.push_back(enemy);
        }
    }
    // check for enemies in the room
    if (!enemies.empty()) 
    {
        std::cout << "Enemies in the room:" << std::endl;
        for (Enemy* enemy : enemies) 
        {
            if (enemy != nullptr)
            {
                std::cout << "- Name: " << enemy->getName() << std::endl;
                std::cout << "  Health: " << enemy->getHealth() << std::endl;
                std::cout << "  Defense: " << enemy->getDefense() << std::endl;
            }
        }
    }
    else 
    {
        std::cout << "There are no enemies in the room." << std::endl;
    }
    // Get npcs in the room
    std::vector<Npc*> npcs;
    for (Npc* npc : location->getNPCs())
    {
        if (npc != nullptr)
        {
            npcs.push_back(npc);
        }
    }
    // check for npcs in the room
    if (!npcs.empty()) 
    {
        std::cout << "NPCs in the room:" << std::endl;
        for (Npc* npc : npcs) 
        {
            if (npc != nullptr)
            {
                std::cout << "- Name: " << npc->getName() << std::endl;
            }
        }
    }
    else 
    {
        std::cout << "There are no NPCs in the room." << std::endl;
    }
}
void Player::interactWithItem(const std::string& itemName) 
{
    // check if the item exists in the current room
    std::vector<Item*> roomItems = location->getItems();

    for (Item* item : roomItems) 
    {
        if (item->getName() == itemName) 
        {
            // prompt the player to decide whether to interact with the item
            std::cout << "Do you want to interact with " << itemName << "? (yes/no): ";
            std::string choice;
            std::cin >> choice;
            if (choice == "yes") 
            {
                // add the item to the players inventory
                addItemToInventory(*item);
                // remove the item from the room
                location->RemoveItem(item);
                // debug message, for successfully picking up item
                std::cout << "You picked up " << itemName << "." << std::endl;
            }
            else if (choice == "no")
            {
                // output that player didnt interact with item
                std::cout << "You chose not to interact with " << itemName << "." << std::endl;
            }
            else 
            {
                // debug message, for invalid choice
                std::cout << "Invalid choice. Please try again." << std::endl;
                
            }
            return;
        }
    }
    // debug message, for no item with name itemName in current room
    std::cout << "There is no item named " << itemName << " in this room." << std::endl;
}
void Player::move(std::string direction) 
{
    // move player to the next room if the direction is valid
    if (getLocation()->getExit(direction) != nullptr)
    {
        // place player in specified location
        setLocation(getLocation()->getExit(direction));
    }
    else
    {
        // debug message, invalid direction
        std::cout << "Invalid direction! Please choose again." << std::endl;
    }
}
void Player::attack(Enemy& enemy) 
{
    // set players current damage to damage
    int currentDamage = getDamage();
    // apply damage reduction if the player is defending
    if (enemy.getEnemyDefending())
    {
        std::cout << "Player would've dealt " << currentDamage 
            << " to enemy, but enemy was defending so: " << std::endl;
        currentDamage = currentDamage - (enemy.getDefense() * 0.5);  // reducing damage by 50% of enemys defense
        enemy.setEnemyDefending(false); // reset defending flag after the attack
    }
    // ensure damage doesnt go below 0
    currentDamage = std::max(currentDamage, 0);
    // apply damage to the enemy
    enemy.takeDamage(currentDamage);
    // output attack message
    std::cout << "You attacked the enemy and dealt " << currentDamage << " damage!" << std::endl;
}
void Player::defend() 
{
    // set the defending flag to true
    playerDefending = true;

    // output player is defending
    std::cout << "You are defending. Incoming damage will be reduced by " <<
        "50% of your defense until your next turn." << std::endl;
}
void Player::use(const std::string& potionName)
{
    // find the potion by name in the players inventory
    Item* potion = nullptr;
    for (Item* item : inventory)
    {
        if (item->getName() == potionName)
        {
            potion = item;
            break;
        }
    }
    // check if the potion exists and is a valid potion
    if (potion != nullptr && potion->getItemType() == Item::ItemType::Potion)
    {
        Potion* potionItem = static_cast<Potion*>(potion);
        int healthRecovered = potionItem->getStatChange();
        // check if using the potion will overfill the players health
        int remainingHealthCapacity = maxHealth - health;
        if (healthRecovered > remainingHealthCapacity)
        {
            healthRecovered = remainingHealthCapacity;
        }
        // update players health
        health += healthRecovered;
        // remove the potion from the players inventory
        removeItemFromInventory(potionName);
        // debug message, successfully used a potion and recovered health
        std::cout << "You used " << potionName << " and recovered " 
            << healthRecovered << " health points." << std::endl;
    }
    else
    {
        // debug message, invalid potion name or not in inventory
        std::cout << "Invalid potion name or potion not found in inventory." << std::endl;
    }
}
void Player::displayEquipment()
{
    // output heading for item in equipment
    std::cout << "Equipment Inventory:" << std::endl;
    // iterate through each item in map equipment
    for (const auto& pair : equipment)
    {
        // output details of each item in equipment
        std::cout << pair.first << ": " << pair.second->getName() << " - " << pair.second->getDescription() << 
            " - Stat Change: " << pair.second->getStatChange() << 
            " - Price: " << pair.second->getPrice() << std::endl;
    }
}
void Player::equip(const std::string& itemName)
{
    // find the item in the players inventory
    auto i = std::find_if(inventory.begin(), inventory.end(), [&](Item* item)
    {
        return item->getName() == itemName;
    });
    // check if the item was found
    if (i == inventory.end())
    {
        // debug message, for item not being found in inventory
        std::cout << "Item not found in inventory." << std::endl;
        return;
    }
    // get a pointer to the item
    Item* itemToEquip = *i;
    // determine the type of the item
    Item::ItemType itemType = itemToEquip->getItemType();
    // determine the appropriate equipment slot based on the items type
    std::string slotName;
    switch (itemType) 
    {
        case Item::ItemType::Weapon:
            // item type is weapon so goes in weapon slot
            slotName = "weapon";
        break;
        case Item::ItemType::Armour:
            // item type is armour so goes in armour slot
            slotName = "armour";
        break;
        case Item::ItemType::Potion:
            // item type is potion, potion isnt a slot, cant be equipped
            std::cout << "Potions cannot be equipped." << std::endl;
        return;
        case Item::ItemType::Unknown:
            // item type is uknown so not correct type
            std::cout << "Unknown item type." << std::endl;
        return;
    }
    // check if the equipment slot is already occupied
    if (equipment[slotName] != nullptr) 
    {
        // debug message, item already in equipped slot
        std::cout << "There is already an item equipped in the " << slotName << " slot." << std::endl;
        return;
    }
    // equip the item to the appropriate equipment slot
    equipment[slotName] = itemToEquip;
    std::cout << itemToEquip->getName() << " equipped to " << slotName << " slot." << std::endl;

    // remove the item from the players inventory
    inventory.erase(i);

    // update player's stats based on the equipped item
    updateStats();
}
void Player::unequip(const std::string& slot)
{
    // check if the slot is valid (e.g., "weapon", "armour")
    if (slot == "weapon" || slot == "armour")
    {
        // check if the slot is occupied
        auto it = equipment.find(slot);
        if (it != equipment.end())
        {
            // add the item back to the players inventory
            inventory.push_back(it->second);

            // remove the item from the equipment inventory
            equipment.erase(it);

            std::cout << "Unequipped item from " << slot << " slot." << std::endl;

            // update players stats after unequipping the item
            updateStats();
        }
        else
        {
            // debug message, no item equipped in slot which youre trying to remove item from
            std::cout << "No item equipped in " << slot << " slot." << std::endl;
        }
    }
    else
    {
        // debug message, invalid slot name
        std::cout << "Invalid equipment slot." << std::endl;
    }
}
