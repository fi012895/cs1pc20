// include required header file
#include "Room.h"
// include required library
#include <string>

// constructor definition
Room::Room(const std::string& description) : description(description) {}

std::string Room::getDescription()
{
    // return description
	return description;
}
void Room::AddItem(Item* item)
{
    // add item to room item vector
	items.push_back(item);
    // debug message, for successfully adding item to room
	std::cout << "Item added: " << item->getName() << " , " << item->getDescription() << std::endl;
	std::cout << std::endl;
}
void Room::RemoveItem(Item* item)
{
    // find item in rooms item vector
    auto it = std::find(items.begin(), items.end(), item);
    if (it != items.end())
    {
        // remove item from rooms item vector
        items.erase(it);
        // debug message, for successfully removing item from room
        std::cout << "Item removed: " << item->getName() << " , " << item->getDescription() << std::endl;
        std::cout << std::endl;
    }
    else
    {
        // debug message, for item not found in room
        std::cout << "Item not found in the room." << std::endl;
    }
}
void Room::AddEnemy(Enemy* enemy)
{
    // add enemy to rooms enemies vector
    enemies.push_back(enemy);
}
void Room::RemoveEnemy(Enemy* enemy)
{
    // find enemy in rooms enemies vector
    auto it = std::find(enemies.begin(), enemies.end(), enemy);
    if (it != enemies.end()) 
    {
        // remove enemy from rooms enemies vector
        enemies.erase(it);
    }
    else
    {
        // debug message, for enemy not found in room
        std::cout << "Enemy not found in the room." << std::endl;
    }
}
void Room::AddNpc(Npc* npc)
{
    // add npc to room
    npcs.push_back(npc);
}
std::vector<Npc*> Room::getNPCs()
{
    // return npcs
    return npcs;
}
std::vector<Item*> Room::getItems()
{
    // return items
    return items;
}
std::vector<Enemy*> Room::getEnemies()
{
    // return enemies
    return enemies;
}
std::map<std::string, Room*>& Room::getExits()
{
    // return map of exits
    return exits; 
}
void  Room::addExit(const std::string& direction, Room* room)
{
    // add the exit to the map
    exits[direction] = room;
    // debug message, for successfully creating exit 
    std::cout << "Created exit: " << direction << " --> " << " from: " << getDescription() << " --> " << room->getDescription() << std::endl;
}
Room* Room::getExit(const std::string& direction)
{
    auto it = exits.find(direction);
    if (it != exits.end())
    {
        // return the room associated with the direction
        return it->second; 
    }
    else
    {
        // return nullptr if the direction is not found
        return nullptr; 
    }
}
void Room::printExits()
{
    // output heading for exits of room
    std::cout << "Exits from " << description << ":" << std::endl;
    // iterate through each exit and print its direction and destination description
    for (const auto& exit : exits) 
    {
        std::cout << "Direction: " << exit.first << ", Destination: " << exit.second->getDescription() << std::endl;
    }
}
