// include required header files
#include "Potion.h"

// constructor definition
Potion::Potion(const std::string& name, const std::string& description, int health, int price)
    : Item(name, description, ItemType::Potion, price), health(health) {}

int Potion::getStatChange() const
{
    // return stat change, health
    return health;
}
