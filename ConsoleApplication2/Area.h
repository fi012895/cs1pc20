#pragma once
// include required header files
#include "Room.h" 
#include "Npc.h" 
// include required libraries
#include <map> 
#include <vector>

// class Area, responsible for anything to do with the play area
class Area
{
    
    private:
        std::map<std::string, Room*> rooms; // map to store rooms with their names
        std::vector<Npc*> npcs; // vector to store Npcs

    public:
        void addRoom(const std::string& name, Room* room); // function to add room to the area
        Room* getRoom(const std::string& name); // function to get a room by its name
        std::map<std::string, Room*>& getRooms(); // function to return reference to rooms map
        std::vector<Npc*>& getNpcs(); // function to return reference to Npcs vector
        void ConnectRooms(const std::string& room1Name, const std::string& // function to connect two rooms
            room2Name, const std::string& direction); 
        void LoadMapFromFile(const std::string& filename); // function to load map data from a file, with file name
        std::string oppositeDirection(const std::string& direction); // function to get opposite direction of given direction, for bidirectional rooms
        std::vector<Item*> readItemsFromFile(const std::string& filename); // function to read items data from a file, with file name
        void randomlyAssignItemsToRooms(const std::vector<Item*>& availableItems); // function to randomly assign items to rooms
        std::vector<Enemy*> readEnemiesFromFile(const std::string& filename); // function to read enemies data from a file, with file name
        void randomlyAssignEnemiesToRooms(const std::vector<Enemy*>& availableEnemies); // function to randomly assign enemies to rooms
        void randomlyAssignNPCsToRooms(Npc& npc); // function to randomly assign Npcs to rooms
        void printRoomsAndExits(); // function to print rooms and their exits 
};