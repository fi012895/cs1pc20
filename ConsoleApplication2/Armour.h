#pragma once
// include required header file of base class
#include "Item.h"

// armour class inheriting from item class, responsible for armour type items
class Armour : public Item
{
	private:
		int defense; // int for defense value assigned to armour

	public:
		Armour(const std::string& name, const std::string& description, int defense, int price); // constructor to initialise armour
		int getStatChange() const override; // overriden function to get stat change, defense
};

