#pragma once
// include required header file of base class
#include "Item.h"

// weapon class inheriting from item class, responsible for weapon type items
class Weapon : public Item
{
	private:
		int damage; // int for damage value assigned to weapon

	public:
		Weapon(const std::string& name, const std::string& description, int damage, int price); // constructor to initialise weapon
		int getStatChange() const override; // overriden function to get stat change, damage

};

