#pragma once

// include required header files
#include "Player.h"
#include "NpcMerchant.h"
#include "Enemy.h" 

// class commandInterpreter, responsible for player, npc and enemy commands
class CommandInterpreter 
{
    private:
        Player* player_; // pointer to player object
        NpcMerchant* merchant_; // pointer to npcmerchant object
        std::map<std::string, Room*>& rooms_; // reference to map of rooms
        
    public:
        CommandInterpreter(Player* player, std::map<std::string, Room*>& rooms, NpcMerchant* merchant); // constructor to initalise commandInterpreter object
        void interpretPlayerCommand(const std::string& command); // function to interpreter player commands
        void interpretEnemyCommand(Enemy* enemy, const std::string& command); // function to interpreter enemy commands
        void interpretNpcCommand(NpcMerchant* npc, const std::string& command); // function to interpreter npc commands
};
