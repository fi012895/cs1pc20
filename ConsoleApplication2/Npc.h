#pragma once
// include required header file of base class
#include "Character.h"
// include required library
#include <vector>

// npc class inheriting from character class, responsible for handling different npcs
class Npc : public Character
{
	private:
		std::vector<std::string> dialogue; // vector for npc dialogue
		std::string dialogueFile; //  string for file containing npc dialogue
	public:
		Npc(const std::string& name, const std::string& dialogueFile, int coins); // constructor to initialise npc
		void loadDialogue(const std::string& filename); // function to load npc dialogue from file
		std::string getRandomDialogue() const; // function to get a random dialogue from the npc dialogue file
};

