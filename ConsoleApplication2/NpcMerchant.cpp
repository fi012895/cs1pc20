// include required header files
#include "NpcMerchant.h"
#include "Character.h"
#include "Weapon.h"
#include "Armour.h"
#include "Potion.h"
// include required libraries
#include <vector>
#include <fstream>
#include <sstream>

// constructor definition
NpcMerchant::NpcMerchant(const std::string& name, const std::string& dialogueFile, int coins)
    : Npc(name, dialogueFile, coins) 
{
    // read items from specified file and add them to merchants inventory
    readNpcItemsFromFile("game_npc_merchant_inventory.txt");
}

void NpcMerchant::readNpcItemsFromFile(const std::string& filename)
{
    // open the file using file name
    std::ifstream file(filename);
    // check file opens successfully
    if (file.is_open())
    {
        std::string line;
        // read each line of the file
        while (std::getline(file, line))
        {
            std::istringstream iss(line);
            std::string itemTypeStr, name, description;
            int statValue, price;

            // parse line to get item details
            if (std::getline(iss, itemTypeStr, '|') && // get itemtype
                std::getline(iss, name, '|') && // get item name
                std::getline(iss, description, '|') && // get item description
                iss >> statValue && // get item statvalue
                iss.ignore() && // ignore |
                iss >> price) // get item price
            {
                // convert string representation of item type to enum ItemType
                Item::ItemType itemType = Item::getItemTypeFromString(itemTypeStr);
                switch (itemType)
                {
                    case Item::ItemType::Weapon:
                        addToInventory(new Weapon(name, description, statValue, price)); // create new weapon object and add it to inventory
                        std::cout << "Added " << name << " to NPC Merchant inventory. Description: " << description << ", Damage: " << statValue << ", Price: " << price << "." << std::endl;
                    break;
                    case Item::ItemType::Armour:
                        addToInventory(new Armour(name, description, statValue, price)); // create new armour object and add it to inventory
                        std::cout << "Added " << name << " to NPC Merchant inventory. Description: " << description << ", Defense: " << statValue << ", Price: " << price << "." << std::endl;
                    break;
                    case Item::ItemType::Potion:
                        addToInventory(new Potion(name, description, statValue, price)); // create new potion object and add it to inventory
                        std::cout << "Added " << name << " to NPC Merchant inventory. Description: " << description << ", Healing: " << statValue << ", Price: " << price << "." << std::endl;
                    break;
                    default:
                        std::cerr << "Invalid item type: " << itemTypeStr << std::endl;
                    break;
                }
            }
        }
        // close file after processing
        file.close();
        // display merchants inventory
        displayInventory();
    }
    else
    {
        // debug message, unable to open file
        std::cerr << "Error: Unable to open file " << filename << std::endl;
    }
}
void NpcMerchant::addToInventory(Item* item)
{
    // add item to merchant inventory
    inventory.push_back(item);
    // debug message, for successfully adding item to merchant inventory
    std::cout << "Added: " << item->getName() << " to merchant inventory." << std::endl;
}
std::vector<Item*>& NpcMerchant::getInventory()
{
    // return merchants inventory
    return inventory;
}
void NpcMerchant::displayInventory()
{
    std::cout << "Merchant's inventory:" << std::endl;
    // get inventory vector
    const std::vector<Item*>& merchantInventory = getInventory();
    // check if the inventory is empty
    if (merchantInventory.empty())
    {
        std::cout << "Merchant's inventory is empty." << std::endl;
        return;
    }
    // iterate through the inventory and display each item
    for (size_t i = 0; i < merchantInventory.size(); ++i)
    {
        if (merchantInventory[i]) // ensure the item pointer is not null
        {
            std::cout << i + 1 << ". " << merchantInventory[i]->getName() << " - "
                << merchantInventory[i]->getDescription() << " - Price: " << merchantInventory[i]->getPrice() << " - ";
            // determine the item type and display the corresponding stat change
            switch (merchantInventory[i]->getItemType())
            {
                case Item::ItemType::Weapon: // item type is weapon
                    // output stat change for weapon
                    std::cout << "Damage: " << dynamic_cast<Weapon*>(merchantInventory[i])->getStatChange();
                break;
                case Item::ItemType::Armour: // item type is armour
                    // output stat change for armour
                    std::cout << "Defense: " << dynamic_cast<Armour*>(merchantInventory[i])->getStatChange();
                break;
                case Item::ItemType::Potion: // item type is potion
                    // output stat change for potion
                    std::cout << "Health: " << dynamic_cast<Potion*>(merchantInventory[i])->getStatChange();
                break;
                default:
                    // unknown item type
                    std::cout << "Unknown";
            }
            std::cout << std::endl;
        }
        else
        {
            // debug message, null item found
            std::cerr << "Null item encountered in inventory!" << std::endl;
        }
    }
}
void NpcMerchant::sellItem(Player& player)
{
    // display the npc merchants inventory
    displayInventory();
    // get the players choice of item to buy
    int choice;
    std::cout << "Enter the index of the item you want to buy: ";
    std::cin >> choice;
    // validate the choice
    if (choice < 1 || choice > inventory.size())
    {
        std::cout << "Invalid choice. Please enter a valid index." << std::endl;
        return;
    }
    // adjust the choice to match vector index
    choice--;
    // get the selected item from the inventory
    Item* itemToSell = inventory[choice];
    // check if the player has enough coins to buy the item
    if (player.getCoins() < itemToSell->getPrice())
    {
        std::cout << "You don't have enough coins to buy this item." << std::endl;
        return;
    }
    // deduct the price of the item from the players coins
    player.setCoins(itemToSell->getPrice(), -1);
    // add the item to the players inventory
    player.addItemToInventory(*itemToSell);
    // display the item bought
    std::cout << "You bought " << itemToSell->getName() << " for " 
        << itemToSell->getPrice() << " coins." << std::endl;
    // remove the item from the npc merchants inventory
    inventory.erase(inventory.begin() + choice);
    // add the price of the item to the npc merchants coins
    coins += itemToSell->getPrice();

}
void NpcMerchant::buyItem(Player& player)
{
    // display the players inventory
    std::cout << "Your inventory:" << std::endl;
    player.displayInventory();
    // get the players choice of item to sell
    int choice;
    std::cout << "Enter the index of the item you want to sell: ";
    std::cin >> choice;
    // validate the choice
    if (choice < 1 || choice > player.getInventory().size())
    {
        std::cout << "Invalid choice. Please enter a valid index." << std::endl;
        return;
    }
    // adjust the choice to match vector index
    choice--;
    // get the selected item from the players inventory
    Item* itemToSell = player.getInventory()[choice];
    // check if the merchant has enough coins to buy the item
    if (coins < itemToSell->getPrice())
    {
        std::cout << "The merchant doesn't have enough coins to buy this item." << std::endl;
        return;
    }
    // deduct the price of the item from the merchants coins
    coins -= itemToSell->getPrice();
    // add the price of the item to the players coins
    player.setCoins(itemToSell->getPrice(), 1);
    // remove the item from the players inventory
    player.removeItemFromInventory(itemToSell->getName());
    // add the item to the merchants inventory
    inventory.push_back(itemToSell);
    std::cout << "The merchant bought " << itemToSell->getName() << " for " 
        << itemToSell->getPrice() << " coins." << std::endl;
}


