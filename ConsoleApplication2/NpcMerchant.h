#pragma once
// include required header file of base class
#include "Npc.h"
#include "Item.h"
#include "Player.h" // for forward declaration
// include required library
#include <vector>
// forward declaration of player class
class Player;

// class npcmerchant, inheriting from npc class, responsible for handling merchant type of npc
class NpcMerchant : public Npc
{
    private:
        std::vector<Item*> inventory; // vector for items in merchants inventory

    public:
        NpcMerchant(const std::string& name, const std::string& dialogueFile, int coins); // constructor to initialise npcmerchant
        void addToInventory(Item* item); // function to add item to inventory using item 
        std::vector<Item*>& getInventory(); // function to get inventory vector
        void displayInventory(); // function to display inventory of merchantnpc
        void readNpcItemsFromFile(const std::string& filename); // function to read items from file and add them to inventory
        void sellItem(Player& player); // function to sell item to player
        void buyItem(Player& player); // function to buy item from player
        
};

