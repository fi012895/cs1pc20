#pragma once
// include required header file of base class
#include "Item.h"

// potion class inheriting from item class, responsible for potion type items
class Potion : public Item
{
	private:
		int health; // int for health value assigned to armour

	public:
		Potion(const std::string& name, const std::string& description, int health, int price); // constructor to initialise potion
		int getStatChange() const override; // overriden function to get stat change, health
};

