// include required header file
#include "CommandInterpreter.h"

// constructor definition
CommandInterpreter::CommandInterpreter(Player* player, std::map<std::string, Room*>& rooms, NpcMerchant* merchant)
    : player_(player), rooms_(rooms), merchant_(merchant) {}


void CommandInterpreter::interpretNpcCommand(NpcMerchant* npc, const std::string& command)
{
    // interpret commands given by npc:
    if (command == "sellItem")
    {
        // call merchants sellItem function with the player, to allow player to buy an item
        merchant_->sellItem(*player_);
    }
    else if (command == "buyItem")
    {
        // call merchants buyItem function with the player, to allow player to sell an item
        merchant_->buyItem(*player_);
    }
    else if (command == "talk") 
    {
        // call merchants getRandomDialogue function to allow npc to talk with player
        merchant_->getRandomDialogue();
    }
}
void CommandInterpreter::interpretPlayerCommand(const std::string& command)
{
    // interpret commands given by player:
    if (command == "help") 
    {
        // display possible commands and their descriptions
        std::cout << "Possible Commands:\n"
            << "  help: Display possible commands.\n"
            << "  investigate: Check surroundings of player.\n"
            << "  interact: Interact with items in room.\n"
            << "  trade: Initiate trading with a merchant.\n"
            << "  talk: Talk to an NPC.\n"
            << "  stats: Display stats.\n"
            << "  battle: Engage in battle with an enemy.\n"
            << "  move: Move to a different room.\n"
            << "  inventory: Access your inventory.\n"
            << "  exit: Exit the game.\n\n";
    }
    else if (command == "talk") 
    {
        // check if there are npcs in the current room
        if (!player_->getLocation()->getNPCs().empty()) 
        {
            // if there are npcs, then allow the player to talk
            interpretNpcCommand(merchant_, "talk");
        }
        else 
        {
            // if there isnt any npc output no npcs to talk to
            std::cout << "There are no NPCs in the room to talk to." << std::endl;
        }
    }
    else if (command == "stats") 
    {
        // call players displayStats function to display their stats
        player_->displayStats();
    }
    else if (command == "trade") 
    {
        // check if there are npcs in the current room
        if (!player_->getLocation()->getNPCs().empty()) 
        {
            // if there are npcs, then allow the player to trade
            while (true) 
            {
                // prompt the player to choose between buying or selling
                std::cout << "What would you like to do? (buy/sell or exit): ";
                std::string tradeOption;
                std::cin >> tradeOption;
                if (tradeOption == "buy") 
                {
                    // call interpretNpcCommand with merchant and sellItem to allow player to buy an item
                    interpretNpcCommand(merchant_, "sellItem");
                }
                else if (tradeOption == "sell") 
                {
                    // call interpretNpcCommand with merchant and buyItem to allow player to sell an item
                    interpretNpcCommand(merchant_, "buyItem");
                }
                else if (tradeOption == "exit") 
                {
                    // exit trade menu 
                    break;
                }
                else 
                {
                    // output invalid trade option if option not from the ones provided
                    std::cout << "Invalid trade option. Please enter 'buy' or 'sell'." << std::endl;
                }
            }
        }
        else 
        {
            // if no npcs in current room, then output no npcs to trade 
            std::cout << "There are no NPCs in the room for trading." << std::endl;
        }
    }
    else if (command == "investigate") 
    {
        // call players investigate function to explore their surroundings
        player_->investigate();
    }
    else if (command == "interact") 
    {
        std::string itemName;
        std::cin.ignore(); // clear the input buffer
        std::cout << "Enter the name of the item you want to interact with: ";
        std::getline(std::cin, itemName); // read the entire line
        // call players interactWithItem function to allow player to interact with item given by item name
        player_->interactWithItem(itemName);
    }
    else if (command == "battle")
    {
        // check if theres an enemy in the current room
        Room* currentRoom = player_->getLocation();
        if (currentRoom != nullptr && !currentRoom->getEnemies().empty())
        {
            // player battles first enemy in room
            Enemy* enemy = currentRoom->getEnemies()[0];
            std::cout << "You find yourself face to face with an enemy! What will you do?" << std::endl;
            // initialize turn counter
            int turn = 1; 
            // battle loop
            while (player_->isAlive() && enemy->isAlive())
            {
                // display turn number and whose turn it is
                std::cout << "Turn " << turn << ": ";
                // players turn
                if (turn % 2 == 1)
                {
                    std::cout << "Player's turn" << std::endl;
                    // display stats of enemy and player
                    enemy->displayStats();
                    player_->displayStats();
                }
                // enemys turn
                else
                {
                    std::cout << "Enemy's turn" << std::endl;
                    // display stats of enemy and player
                    enemy->displayStats();
                    player_->displayStats();
                }

                // players turn
                if (turn % 2 == 1)
                {
                    // prompt choice 
                    std::cout << "Choose your action: (attack/defend/use): ";
                    std::string battleOption;
                    std::cin >> battleOption;
                    // attack
                    if (battleOption == "attack")
                    {
                        std::cout << "You prepare to strike!" << std::endl;
                        // call players attack function with enemy, to deal damage to enemy
                        player_->attack(*enemy);
                    }
                    // defend
                    else if (battleOption == "defend")
                    {
                        std::cout << "You brace yourself for the enemy's attack!" << std::endl;
                        // call players defend function to decrease enemy damage
                        player_->defend();
                    }
                    // use item
                    else if (battleOption == "use")
                    {
                        player_->displayInventory();
                        std::cin.ignore(); // clear the input buffer
                        // Prompt the player to choose a potion to use
                        std::cout << "Which potion would you like to use? ";
                        std::string potionName;
                        std::getline(std::cin, potionName); // read the entire line
                        // call players use function with potion name, to use the potion to heal
                        player_->use(potionName);
                    }
                    else
                    {
                        std::cout << "Invalid action. Please choose 'attack', 'defend', 'use' or 'run'." << std::endl;
                    }
                }
                // enemys turn
                else 
                {
                    // seed the random number generator
                    srand(time(0));
                    // generate a random number between 0 and 99
                    int randomChance = rand() % 100;
                    // determine the enemys action based on the random chance
                    if (randomChance < 60) // 60% chance to attack
                    {
                        // call interpretEnemyCommand with enemy and "attack" to allow enemy to attack player
                        interpretEnemyCommand(enemy, "attack");
                    }
                    else if (randomChance < 90) // 30% chance to defend (60 + 30 = 90)
                    {
                        // call interpretEnemyCommand with enemy and "defend" to allow enemy to reduce player damage
                        interpretEnemyCommand(enemy, "defend");
                    }
                    else // 10% chance to hesitate 
                    {
                        // call interpretEnemyCommand with enemy and "hesitate" where enemy does nothing
                        interpretEnemyCommand(enemy, "hesitate");
                    }
                }
                // increment turn counter
                turn++;
                // check if the enemy is defeated
                if (!enemy->isAlive())
                {
                    std::cout << "You have defeated the enemy!" << std::endl;
                    // enemy drops coins it was holding 
                    std::cout << "The enemy dropped " << enemy->getCoins() << " coins." << std::endl;
                    // player picks up coins, increment players coins
                    player_->setCoins(enemy->getCoins(), 1);
                    break;
                }
                // check if the player is defeated
                if (!player_->isAlive())
                {
                    std::cout << "You have been defeated!" << std::endl;
                    break;
                }
            }
        }
        else
        {
            std::cout << "There are no enemies in this room." << std::endl;
        }
    }
    else if (command == "move") 
    {
        // get the current room where the player is located
        Room* currentRoom = player_->getLocation();
        // prompt the player to choose which direction to move to
        std::cout << "Where would you like to go? (north/east/south/west): ";
        std::string moveOption;
        std::cin >> moveOption;
        // call players move function with moveoption (direction) to move the player in direction
        player_->move(moveOption);
    }
    else if (command == "inventory") 
    {
        // loop to handle inventory commands
        while (true)
        {
            // prompt player to choose inventory action
            std::cout << "What would you like to do? (open/remove/equip/unequip or exit): ";
            std::string inventoryOption;
            std::cin >> inventoryOption;

            if (inventoryOption == "open")
            {
                // call players displayInventory function to display items in inventory
                player_->displayInventory();
            }
            else if (inventoryOption == "remove")
            {
                // call players displayInventory function to display items in inventory
                player_->displayInventory();
                std::cin.ignore(); // clear the input buffer
                // prompt player for item name they would like to remove from inventory
                std::cout << "What is the name of the item you would like to remove: " << std::endl;
                std::string name;
                std::getline(std::cin, name); // read the entire line
                // call players removeItemFromInventory function with name, to remove the item from their inventory with name
                player_->removeItemFromInventory(name);
            }
            else if (inventoryOption == "equip")
            {
                // call players displayInventory function to display items in inventory
                player_->displayInventory();
                std::cin.ignore(); // clear the input buffer
                // prompt player for item name they would like to equip from inventory
                std::cout << "What is the name of the item you would like to equip: ";
                std::string name;
                std::getline(std::cin, name); // read the entire line
                // call players equip function with name, to equip item with name to a slot
                player_->equip(name);
                // call players displayEquipment function to display items equipped in equipment slots
                player_->displayEquipment();
            }
            else if (inventoryOption == "unequip")
            {
                // call players displayEquipment function to display items equipped in equipment slots
                player_->displayEquipment();
                std::cin.ignore(); // clear the input buffer
                // prompt player for slot name they would like to remove item from
                std::cout << "What equipment slot would you like to unequip from (weapon/armour): ";
                std::string slot;
                std::getline(std::cin, slot); // read the entire line
                // call players unequip function with slot to unequip the item in the slot
                player_->unequip(slot); 
            }
            else if (inventoryOption == "exit") 
            {
                break; // exit loop
            }
            else
            {
                // debug message, for invalid inventory option
                std::cout << "Invalid inventory option. Please enter 'open' or 'remove' or 'equip' or 'unequip'." << std::endl;
            }
        }
    }
    else if (command == "exit") 
    {
        // output exiting game
        std::cout << "Exiting the game. Goodbye!" << std::endl;
    }
    else 
    {
        // debug message, for invalid command
        std::cout << "Invalid command. Please try again." << std::endl;
        return;
    }
}
void CommandInterpreter::interpretEnemyCommand(Enemy* enemy, const std::string& command) 
{
    // interpret commands given by enemy:
    if (command == "attack") 
    {
        // call enemys attackPlayer function with player to allow enemy attacking player
        enemy->attackPlayer(*player_);
    }
    else if (command == "defend") 
    {
        // call enemys defend function to allow enemy defending
        enemy->defend();
    }
    else if (command == "hesitate") 
    {
        // output message of enemy hesitating, do nothing
        std::cout << "The enemy hesitates, unsure of its next move." << std::endl;
    }
}



