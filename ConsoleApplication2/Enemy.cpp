// include required header file
#include "Enemy.h"

// constructor definition
Enemy::Enemy(const std::string& name, const std::string& description, int health, int damage, int defense, int coins)
    : Character(name, health, defense, damage, coins, description, location) {}

void Enemy::attackPlayer(Player& player)
{
    // set damage to enemies damage value
    int damage = getDamage();
    // apply damage reduction if the player is defending
    if (player.getPlayerDefending())
    {
        // debug message, for checking correct damage reduction
        std::cout << "Enemy would've dealt " << damage 
            << " to player, but player was defending so: " << std::endl;
        damage = damage - (player.getDefense() * 0.5); // reducing damage by 50% of player's defense

        player.setPlayerDefending(false); // reset defending flag after the attack
    }
    // ensure damage doesn't go below 0
    damage = std::max(damage, 0);
    // apply damage to the player
    player.takeDamage(damage);
    // output attack message
    std::cout << "The enemy attacked you and dealt " << damage << " damage!" << std::endl;
}
void Enemy::defend() 
{
    // set the defending flag to true
    enemyDefending = true;
    // output enemy is defending
    std::cout << "The enemy is defending. Incoming damage will be " << 
        "reduced by 50 % of its defense until its next turn." << std::endl;
}
bool Enemy::getEnemyDefending()
{
    // return if enemy is defending or not
    return enemyDefending;
}
void Enemy::setEnemyDefending(bool defending) 
{
    // set if enemy is defending or not
    enemyDefending = defending;
}

