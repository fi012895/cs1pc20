// include required header files
#include "Weapon.h"

// constructor definition
Weapon::Weapon(const std::string& name, const std::string& description, int damage, int price) 
	: Item(name, description, ItemType::Weapon, price), damage(damage) {}

int Weapon::getStatChange() const
{
	// return stat change, damage
	return damage;
}
