// include required header files
#include "Armour.h"

// constructor definition
Armour::Armour(const std::string& name, const std::string& description, int defense, int price)
    : Item(name, description, ItemType::Armour, price), defense(defense) {}

int Armour::getStatChange() const
{
    // return stat change, defense
    return defense; 
}
