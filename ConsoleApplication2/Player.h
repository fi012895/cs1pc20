#pragma once
// include required header files of base class
#include "Character.h"
#include "Npc.h"
#include "NpcMerchant.h"
#include "Enemy.h"
#include "Item.h"
#include "Room.h"
// include required library
#include <vector>
// forward declaration of Npc and NpcMerchant class
class Npc;
class NpcMerchant;

// class player inheriting from character class, responsible for handling players
class Player : public Character
{
	private:
		std::vector<Item*> inventory; // vector for players inventory
		std::map<std::string, Item*> equipment; // map for players equipment
		int maxHealth = health; // int for maximum health of player
		bool playerDefending = false; // bool flag for indicating if player is defending or not
		int currentDamage = damage; // int for current damage of player
		int currentDefense = defense; // int for current defense of player

	public:
		Player(const std::string& name, std::string description, Room* location, int damage, int defense, int health); // constructor to initialise armour
		std::vector<Item*>& getInventory(); // function to get the players inventory
		void addItemToInventory(Item& item); // function to add item to players inventory
		void removeItemFromInventory(const std::string& itemName); // function to remove item from players inventory
		void displayInventory(); // function to display players inventory
		void displayEquipment(); // function to display players equipment
		void displayStats() override; // function to display players stats
		void updateStats(); // function to update players stats
		int getDefense() override; // function to get players defense
		int getDamage() override; // function to get players damage
		void investigate(); // function to allow player to investigate surroundings in room
		void interactWithItem(const std::string& itemName); // function to allow player to interact with item using itemname
		void move(std::string direction); // function to allow player to move to room in a direction
		void attack(Enemy& enemy); // function to allow player to attack an enemy
		void defend(); // function to allow player to defend, decrease damage taken
		void equip(const std::string& itemName); // function to allow player to equip item from inventory to equipment slot using itemname
		void unequip(const std::string& slot); // function to allow player to unequip item from equipment using slot and place it back in inventory
		void use(const std::string& potionName); // function to allow player to use a potion from inventory during battle
		bool getPlayerDefending(); // function to get if player is defending or not
		void setPlayerDefending(bool defending); // function to set if player is defending or not
};

