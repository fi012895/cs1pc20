// include required header file
#include "Npc.h"
// include required libraries
#include <fstream>
#include <random>

// constructor definition
Npc::Npc(const std::string& name, const std::string& dialogueFile, int coins)
    : Character(name, health, defense, damage, coins, description, nullptr), dialogueFile(dialogueFile)
{
    // load npc dialogue from specified file
    loadDialogue(dialogueFile);
}
    
void Npc::loadDialogue(const std::string& filename) 
{
    // open dialogue file
    std::ifstream file(filename);
    // check if file opened successfully
    if (file.is_open())
    {
        std::string line;
        // read each line of the file and add to npc dialogue vector
        while (std::getline(file, line))
        {
            // add line to dialogue vector
            dialogue.push_back(line);
            // debug message, for successfully adding line to the vector
            std::cout << "Added dialogue: " << line << std::endl;

        }
        // close file once processed
        file.close();
    }
    else
    {
        // debug message, unable to open the file
        std::cerr << "Error: Unable to open dialogue file " << filename << std::endl;
    }
}
std::string Npc::getRandomDialogue() const 
{
    // check if the dialogue vector is empty
    if (dialogue.empty())
    {
        return "No dialogue available.";
    }

    // generate a random index within the range of the dialogue vector
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<int> dis(0, (dialogue.size() - 1));
    int index = dis(gen);

    // return the dialogue at the random index
    return dialogue[index];
}