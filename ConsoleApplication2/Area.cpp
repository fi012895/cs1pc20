// include required header files
#include "Area.h"
#include "Item.h"
#include "Weapon.h"
#include "Armour.h"
#include "Potion.h"
#include "Enemy.h"
#include "Room.h"
// include required libraries
#include <fstream>
#include <sstream>
#include <random>
#include <algorithm>

void Area::addRoom(const std::string& name, Room* room) 
{
    // create a new Room object
    Room* newRoom = new Room(name);
    // output debug message, for successful creation of new Room object with Name = name
    std::cout << "Added new room: " << name << std::endl;
    // add the new Room object to the map
    rooms[name] = newRoom;
    
}
Room* Area::getRoom(const std::string& name)
{
    // find the room with the specified name in the map
    auto it = rooms.find(name); 
    if (it != rooms.end()) 
    {
        // return the pointer to the room if found
        return it->second; 
    }
    else 
    {
        // output debug message, for unsuccessful room finding
        std::cerr << "Error: Room '" << name << "' not found." << std::endl;
        return nullptr; // return nullptr if room not found
    }
}
std::map<std::string, Room*>& Area::getRooms()
{
    // return map of rooms
    return rooms;
}
std::vector<Npc*>& Area::getNpcs()
{
    // return vector of npcs
    return npcs;
}
std::string Area::oppositeDirection(const std::string& direction)
{
    // initialise variable for opposite direction
    std::string oppositeDir = "";
    // north -> south
    if (direction == "north") 
    { 
        oppositeDir = "south"; 
    }
    // south -> north
    else if (direction == "south") 
    { 
        oppositeDir = "north"; 
    }
    // east -> west
    else if (direction == "east") 
    {
        oppositeDir = "west"; 
    }
    // west -> east
    else if (direction == "west") 
    { 
        oppositeDir = "east"; 
    }
    // invalid direction
    else
    {
        // debug message, for invalid direction
        std::cerr << "Invalid direction: " << direction << std::endl;
        oppositeDir = ""; // Set oppositeDir to a default value
    }
    // debug message, for checking correct directions 
    std::cout << "Opposite direction of " << direction << " is " << oppositeDir << std::endl;
    // return the opposite direction of input
    return oppositeDir;
}
void Area::ConnectRooms(const std::string& room1Name, const std::string& room2Name, const std::string& direction)
{
    // create object for room1 and room2 which are going to be connected, using room names
    Room* room1 = getRoom(room1Name);
    Room* room2 = getRoom(room2Name);
    // check both rooms exist
    if (room1 != nullptr && room2 != nullptr)
    {
        // add exit from room1 to room2 using input direction
        room1->addExit(direction, room2);
        // debug message, for successful connection of rooms and outputing the exits from room1
        std::cout << "Connected " << room1Name << " to " << room2Name << " in the " << direction << " direction." << std::endl;
        std::cout << "Available exits from " << room1Name << ": ";
        // iterate over exits of room1
        for (const auto& exit : room1->getExits())
        {
            // output direction of exit
            std::cout << exit.first << " ";
        }
        std::cout << std::endl;
        // get opposite direction of input direction using oppositeDirection(direction)
        std::string oppositeDir = oppositeDirection(direction);
        // add exit from room2 to room1 using the opposite direction, add bidirectionality to rooms
        room2->addExit(oppositeDir, room1);
        // debug message, for successful connection of rooms and outputting the exits from room2
        std::cout << "Connected " << room2Name << " to " << room1Name << " in the " << oppositeDir << " direction." << std::endl;
        std::cout << "Available exits from " << room2Name << ": ";
        // iterate over exits of room2
        for (const auto& exit : room2->getExits())
        {
            // output direction of exit
            std::cout << exit.first << " ";
        }
        std::cout << std::endl;
    }
    else
    {
        // debug message, for error in conencting rooms, for room1 or room2 not being found, (i.e one of them = nullptr)
        std::cerr << "Error: Unable to connect rooms. One or more rooms not found." << std::endl;
    }
}
void Area::LoadMapFromFile(const std::string& filename)
{
    // open file with file name
    std::ifstream file(filename);
    // check if file is successfully opened
    if (file.is_open())
    {
        std::string line;
        // read each line from the file 
        while (std::getline(file, line))
        {
            std::istringstream iss(line);
            std::string room1Name, room2Name, direction;
            // read room names and direction directly, trimming whitespace characters
            std::getline(iss >> std::ws, room1Name, '|'); // get the first room name from the line
            std::getline(iss >> std::ws, room2Name, '|'); // get the second room name from the line
            std::getline(iss >> std::ws, direction); // get the direction from the line
            // create first room from its name
            Room* room1 = getRoom(room1Name);
            // if room one doesnt exist, create new one
            if (!room1) 
            {
                room1 = new Room(room1Name);
                addRoom(room1Name, room1);
            }
            // create second room from its name
            Room* room2 = getRoom(room2Name);
            // if room two doesnt exist, create new one
            if (!room2) 
            {
                room2 = new Room(room2Name);
                addRoom(room2Name, room2);
            }

            // connect rooms based on the parsed data
            ConnectRooms(room1Name, room2Name, direction);
        }
        // close file after processing
        file.close();
    }
    else
    {
        // debug message, for unable to open the file
        std::cerr << "Error: Unable to open file " << filename << std::endl;
    }
    // output rooms and their exits after loading the map from the file
    printRoomsAndExits();
}
void Area::randomlyAssignItemsToRooms(const std::vector<Item*>& availableItems)
{
    std::random_device rd; // get a random number
    std::mt19937 gen(rd()); // seed the random number generator
    std::uniform_int_distribution<int> dis(0, (availableItems.size() - 1)); // define distribution
    // iterate through each room in the area
    for (const auto& roomPair : rooms)
    {
        // get pointer to current room in iteration
        Room* room = roomPair.second;
        // randomly select an index within range of available items
        int index = dis(gen); // generate random index
        // check index is in bounds
        if (index < availableItems.size())
        {
            // get item at random using index
            Item* item = availableItems[index];
            // debug message, for successfully adding item to room 
            std::cout << "Added Item: " << item->getName() << " to room: " << room->getDescription() << std::endl;
            // add item based on index to the current room
            room->AddItem(item);
        }
    }
}
std::vector<Item*> Area::readItemsFromFile(const std::string& filename)
{
    // vector to store items read from the file using file name
    std::vector<Item*> items;
    // open the file using file name
    std::ifstream file(filename);
    // check if file is successfully open
    if (file.is_open())
    {
        std::string line;
        // read each line from the file
        while (std::getline(file, line))
        {
            std::istringstream iss(line);
            std::string itemTypeStr, name, description;
            int statValue, price;
            // get item data from the line
            if (std::getline(iss, itemTypeStr, '|') &&
                std::getline(iss, name, '|') &&
                std::getline(iss, description, '|') &&
                iss >> statValue && // get stat value
                iss.ignore() && // ignore | 
                iss >> price)// get price
            {
                // determine the item type from the string representation
                Item::ItemType itemType = Item::getItemTypeFromString(itemTypeStr);
                // create new item object based on its type and add to items vector
                switch (itemType)
                {
                    case Item::ItemType::Weapon:
                        items.push_back(new Weapon(name, description, statValue, price)); // create new weapon object
                        // debug message, for successfully adding weapon with given name, description, damage, price
                        std::cout << "Added weapon: " << name << ", " << description << ", Damage: " << statValue << ", Price: " << price << "." << std::endl;
                    break;
                    case Item::ItemType::Armour:
                        items.push_back(new Armour(name, description, statValue, price)); // create new armour object
                        // debug message, for successfully adding armour with given name, description, defense, price
                        std::cout << "Added armor: " << name << ", " << description << ", Defense: " << statValue << ", Price: " << price << "." << std::endl;
                    break;
                    case Item::ItemType::Potion:
                        items.push_back(new Potion(name, description, statValue, price)); // create new potion object
                        // debug message, for successfully adding potion with given name, description, healing, price
                        std::cout << "Added potion: " << name << ", " << description << ", Healing: " << statValue << ", Price: " << price << "." << std::endl;
                    break;
                    default:
                        // debug message, for invalid item type, not weapon,armour or potion
                        std::cerr << "Invalid item type: " << itemTypeStr << std::endl;
                    break;
                }
            }
        }
        // close file after processing
        file.close();
    }
    else
    {
        // debug message, for unable to open the file
        std::cerr << "Error: Unable to open file " << filename << std::endl;
    }
    // return vector of items read from the file
    return items;
}
std::vector<Enemy*> Area::readEnemiesFromFile(const std::string& filename) 
{
    // vector to store enemies read from file using file name
    std::vector<Enemy*> enemies;
    // open the file using file name
    std::ifstream file(filename);
    // check if file is successfully open
    if (file.is_open()) 
    {
        std::string line;
        // read each line from the file 
        while (std::getline(file, line)) 
        {
            std::istringstream iss(line);
            std::string name, description;
            int health = 0, damage = 0, defense = 0, coins = 0;
            // get enemy data from the line
            if (std::getline(iss, name, '|') && // get enemy name
                std::getline(iss, description, '|') && // get enemy description
                iss >> health && // get enemy health
                iss.ignore() && // ignore |
                iss >> damage && // get enemy damage
                iss.ignore() && // ignore |
                iss >> defense && // get enemy defense
                iss.ignore() && // ignore |
                iss >> coins) // get enemy coins
            {
                // create new enemy object and add it to the vector
                enemies.push_back(new Enemy(name, description, health, damage, defense, coins));
                // debug message, for successufully adding enemy with given name, description, health, damage, defense, coins
                std::cout << "Added enemy: " << name << ", " << description << ", Health: " << health
                    << ", Damage: " << damage << ", Defense: " << defense << ", Coins: " << coins << "." << std::endl;
            }
            else 
            {
                // debug message, for unable to parse enemy from a line
                std::cerr << "Error: Unable to parse enemy from line: " << line << std::endl;
            }
        }
        // close file after processing
        file.close();
    }
    else 
    {
        // debug message, for unable to open the file
        std::cerr << "Error: Unable to open file " << filename << std::endl;
    }
    // return vector of enemies read from the file
    return enemies;
}
void Area::randomlyAssignEnemiesToRooms(const std::vector<Enemy*>& availableEnemies)
{
    // check if there are enemies or rooms in area
    if (availableEnemies.empty() || rooms.empty())
    {
        // debug message, for no enemies or rooms available in the area
        std::cerr << "Error: No available enemies or rooms in the area." << std::endl;
        return; // exit function
    }
    // seed the random number generator
    std::srand(std::time(nullptr));
    // iterate through each room in the area
    for (const auto& pair : rooms)
    {
        // get pointer to current room
        Room* room = pair.second;
        // check if room already has enemy
        if (!room->getEnemies().empty())
        {
            // debug message, for room already having an enemy
            std::cout << "Room '" << room->getDescription() << "' already has enemies assigned." << std::endl;
            continue; // skip to next iteration
        }
        // randomly get an enemy from vector availableEnemies
        int index = std::rand() % availableEnemies.size(); // generate random index
        // get a pointer to randomly selected enemy
        Enemy* enemy = availableEnemies[index];
        // add selected enemy to the current room
        room->AddEnemy(enemy);
        // debug message, for successfulyy assigning enemy to a room
        std::cout << "Assigned enemy '" << enemy->getName() << "' to room '" << room->getDescription() << "'." << std::endl;
    }
    // debug message, for successfully randomly assigning enemies to rooms
    std::cout << "Enemies have been randomly assigned to rooms." << std::endl;
}
void Area::randomlyAssignNPCsToRooms(Npc& npc)
{
    // check if there are any available rooms
    if (rooms.empty()) 
    {
        // debug message, for no rooms available to add an Npc
        std::cout << "No rooms available to add NPC." << std::endl;
        return; // exit function
    }

    // seed the random number generator
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<int> distribution(0, rooms.size() - 1);
    // generate random index within range of available rooms
    int index = distribution(gen);
    // get iterator to the randomly selected room
    auto it = rooms.begin();
    std::advance(it, index);
    // get pointer to the randmoly selected room
    Room* selectedRoom = it->second;
    // add Npc to randomly selected room
    selectedRoom->AddNpc(&npc);
    // debug message, for successfully adding Npc to a room
    std::cout << "NPC added to room: " << selectedRoom->getDescription() << std::endl;
}
void Area::printRoomsAndExits() 
{
    // header indicating Rooms and Exits
    std::cout << "Rooms and Exits:" << std::endl;
    // iterate through each room in the area
    for (const auto& roomPair : rooms) 
    {
        // get a pointer to the current room
        Room* room = roomPair.second;
        // output description of current room
        std::cout << "Room: " << room->getDescription() << std::endl;
        // output exits for current room
        std::cout << "Exits:" << std::endl;
        // iterate through each exit in the current room
        for (const auto& exitPair : room->getExits()) 
        {
            // output direction and destination of each exit
            std::cout << "Direction: " << exitPair.first << ", Destination: " << exitPair.second->getDescription() << std::endl;
        }
        std::cout << std::endl;
    }
}