#pragma once
// include required header files of base class
#include "Character.h"
#include "Player.h" // for forward declaration
// forward declaration of player class
class Player;

// enemy class inheriting from character class, responsible for handling enemies
class Enemy : public Character
{
	private:
		bool enemyDefending = false; // bool for enemyDefending, for if enemy defends or not

	public:
		Enemy(const std::string& name, const std::string& description, int health, int damage, int defense, int coins); // constructor to initialise enemy
		void attackPlayer(Player& player); // function to attack player 
		void defend(); // function to defend, decreasing damage taken 
		bool getEnemyDefending(); // function to get if enemy is defending or not
		void setEnemyDefending(bool defending); // function to set if enemy is defending or not
};

